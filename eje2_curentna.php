 <?php 

class Empleado{
    private $salarioN;
    private $renta;
    private $totaldes;
    private $horaex;
    private $totalsal;
    private $seguroS;
    private $afp;
  


    public function getTotalSal():float{
        return $this->totalsal;
    }	

    public function setTotalSalario(float $TOTAL){
        $this->totalsal=$TOTAL;
    }
    public function getSalarioN():float{
        return $this->salarioN;
    }	
    public function setSalarioN(float $SUELDO){
        $this->salarioN=$SUELDO;
    }	
    
    public function getSeguroSocial():float{
        return $this->seguroS;
    }	
    public function setSeguro(float $segurosocialISSS){
        $this->seguroS=$segurosocialISSS;
    }	

    public function getRenta():float{
        return $this->renta;
    }	
    public function setRenta(float $RENTA){
        $this->renta=$RENTA;
    }

    public function getHoras():float{
        return $this->horaex;
    }	

    public function setHorasExtras(float $HORASEX){
        $this->horaex=$HORASEX;
    }


    public function getDescuentos():float{
        return $this->totaldes;
    }	

    public function setTotalDescuentos(float $TotalDes){
        $this->totaldes=$TotalDes;
    }
 
 
    public function getdesAfp():float{
        return $this->afp;
    }	
    public function setAfp(float $DESAFP){
        $this->afp=$DESAFP;
    }	

  

    ///////////////////////////////////////////////////////////////////////////////////
    public function TotalDescuentos(){
        $totalDes = $this->afp + $this->seguroS + $this->renta;
        $this->setTotalDescuentos($totalDes);
    }

    public function DescuentoAFP(float $AFP){
        $desAFP = $this->salarioN * $AFP ;
        $this->setAfp($desAFP);
    }

    public function DescuentoISS(float $ISSS){
        $desISSS = $this->salarioN * $ISSS ;
        $this->setSeguro($desISSS);
    }

  

    

    public function CRenta(){
        $renta = 0;
        $coutafija = 0;
        $exceso = 0;

        if($this->salarioN >=0.01 && $this->salarioN <=472.00){
            $renta = 0;
            $coutafija = 0;
            $retencion = 0;
        }else if($this->salarioN >=472.01 && $this->salarioN <=895.24){
            $coutafija = 17.67;
            $retencion = ($this->salarioN - 472) * 0.1;
            $renta = $retencion + $coutafija;
        }else if($this->salarioN >=895.25 && $this->salarioN <=2038.10){
            $coutafija = 60;
            $retencion = $this->salarioN - 895.25 * 0.2;
            $renta = $retencion + $coutafija;   
        }else{
            $coutafija = 288.57;
            $retencion = $this->salarioN- 2038.11 * 0.3;
            $renta = $retencion + $coutafija;   
        }
        $this->setRenta($renta);
    }

    public function TotalSalario(){
        $totalsal = $this->salarioN + $this->horaex - $this->totaldes;
        $this->setTotalSalario($totalsal);
    }

    public function HorasExtras(float $HORASEXTRAS){
        $pago = 10;
        $total = $HORASEXTRAS * $pago;
        $this->setHorasExtras($total);
    }
  
  
    
}
$boletapago = new Empleado();
$boletapago->setSalarioN(800.00);
$boletapago->DescuentoISS(0.075);
$boletapago->DescuentoAFP(0.035);
$boletapago->HorasExtras(5);
$boletapago->CRenta(); 
$boletapago->TotalDescuentos();
$boletapago->TotalSalario();
echo "El sueldo del empleado es de:" , $boletapago->getSalarioN();
echo "</br> Descuento de ISSS es de: ",$boletapago->getSeguroSocial();
echo "</br> Descuento de AFP es de:" , $boletapago->getdesAfp();
echo "</br> Descuento de renta es de: " , $boletapago->getRenta();
echo "</br> Total de descuentos: " , $boletapago->getDescuentos();
echo "</br> Total de pago por horas extras: " , $boletapago->getHoras();
echo "</br>  Sueldo liquido del empleado es de: " , $boletapago->getTotalSal();
?>